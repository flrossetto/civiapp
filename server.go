package main

import (
	"encoding/json"
	"net/http"
	"sort"
	"strconv"

	"github.com/pkg/errors"
)

type Points interface {
	Filter(ref Point, maxDistance int) []PointWithDistance
}

type Server struct {
	points Points
}

func NewServer(points Points) *Server {
	return &Server{points}
}

func (s *Server) PointsHandler(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()

	maxDistance, err := strconv.Atoi(q.Get("distance"))
	if err != nil {
		writeResponse(w, http.StatusBadRequest, errors.New("invalid distance"))
		return
	}

	x, err := strconv.Atoi(q.Get("x"))
	if err != nil {
		writeResponse(w, http.StatusBadRequest, errors.New("invalid x"))
		return
	}

	y, err := strconv.Atoi(q.Get("y"))
	if err != nil {
		writeResponse(w, http.StatusBadRequest, errors.New("invalid y"))
		return
	}

	points := s.points.Filter(Point{X: x, Y: y}, maxDistance)

	sort.SliceStable(points, func(i, j int) bool {
		return points[i].Distance < points[j].Distance
	})

	writeResponse(w, http.StatusOK, points)
}

func writeResponse(w http.ResponseWriter, status int, content interface{}) {
	w.WriteHeader(status)
	w.Header().Add("Content-Type", "application/json")

	if err, ok := content.(error); ok {
		json.NewEncoder(w).Encode(map[string]string{"error": err.Error()})
		return
	}

	json.NewEncoder(w).Encode(content)
}
