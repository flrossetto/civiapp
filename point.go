package main

type Point struct {
	X int `json:"x"`
	Y int `json:"y"`
}

type PointWithDistance struct {
	X        int `json:"x"`
	Y        int `json:"y"`
	Distance int `json:"distance"`
}

func (p *Point) Distance(ref Point) int {
	var distance int
	if p.X >= ref.X {
		distance = p.X - ref.X
	} else {
		distance = ref.X - p.X
	}

	if p.Y >= ref.Y {
		distance += p.Y - ref.Y
	} else {
		distance += ref.Y - p.Y
	}

	return distance
}

type PointsImp []Point

func (p PointsImp) Filter(ref Point, maxDistance int) []PointWithDistance {
	result := []PointWithDistance{}

	for _, point := range p {
		if distance := point.Distance(ref); distance <= maxDistance {
			result = append(result, PointWithDistance{X: point.X, Y: point.Y, Distance: distance})
		}
	}

	return result
}
