package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPoint_Distance(t *testing.T) {
	t.Run("x0 - y0", func(t *testing.T) {
		pointRef := Point{X: 0, Y: 0}
		assert.Equal(t, 0, pointRef.Distance(Point{X: 0, Y: 0}))
		assert.Equal(t, 20, pointRef.Distance(Point{X: 10, Y: 10}))
		assert.Equal(t, 20, pointRef.Distance(Point{X: 10, Y: -10}))
		assert.Equal(t, 20, pointRef.Distance(Point{X: -10, Y: 10}))
		assert.Equal(t, 20, pointRef.Distance(Point{X: -10, Y: -10}))
	})

	t.Run("x6 - y-6", func(t *testing.T) {
		pointRef := Point{X: 6, Y: -6}

		assert.Equal(t, 12, pointRef.Distance(Point{X: 0, Y: 0}))
		assert.Equal(t, 16, pointRef.Distance(Point{X: 8, Y: 8}))
		assert.Equal(t, 4, pointRef.Distance(Point{X: 8, Y: -8}))
		assert.Equal(t, 28, pointRef.Distance(Point{X: -8, Y: 8}))
		assert.Equal(t, 16, pointRef.Distance(Point{X: -8, Y: -8}))
	})

}

func TestPointsImp_Filter(t *testing.T) {
	t.Run("empty", func(t *testing.T) {
		points := PointsImp{}
		filtered := points.Filter(Point{X: 6, Y: -6}, 12)
		assert.Equal(t, []PointWithDistance{}, filtered)
	})

	t.Run("ok", func(t *testing.T) {
		points := PointsImp{
			{X: 8, Y: 8},
			{X: 10, Y: 5},
			{X: -1, Y: -2},
			{X: 7, Y: -3},
			{X: 16, Y: -1},
			{X: 2, Y: -1},
			{X: -12, Y: 9},
		}

		expected := []PointWithDistance{
			{X: -1, Y: -2, Distance: 11},
			{X: 7, Y: -3, Distance: 4},
			{X: 2, Y: -1, Distance: 9},
		}

		filtered := points.Filter(Point{X: 6, Y: -6}, 12)

		assert.Equal(t, expected, filtered)
	})
}
