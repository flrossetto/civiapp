package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

func main() {
	data, err := ioutil.ReadFile("./points.json")
	if err != nil {
		panic(err)
	}

	var points Points
	if err := json.Unmarshal(data, &points); err != nil {
		panic(err)
	}

	server := NewServer(points)

	http.HandleFunc("/api/points", server.PointsHandler)
	http.ListenAndServe(":3000", nil)
}
