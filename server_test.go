package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type PointsMock struct {
	mock.Mock
}

func (m *PointsMock) Filter(ref Point, maxDistance int) []PointWithDistance {
	args := m.Called(ref, maxDistance)
	result, _ := args.Get(0).([]PointWithDistance)
	return result
}

func Test_returnError(t *testing.T) {
	t.Run("bad gateway", func(t *testing.T) {
		w := httptest.NewRecorder()

		writeResponse(w, http.StatusBadGateway, errors.New("Status: BadGateway"))

		res := w.Result()
		defer res.Body.Close()

		data, err := ioutil.ReadAll(res.Body)
		assert.NoError(t, err)
		assert.Equal(t, http.StatusBadGateway, res.StatusCode)
		assert.JSONEq(t, `{"error": "Status: BadGateway"}`, string(data))
	})

	t.Run("not implemented", func(t *testing.T) {
		w := httptest.NewRecorder()

		writeResponse(w, http.StatusNotImplemented, errors.New("Status: NotImplemented"))

		res := w.Result()
		defer res.Body.Close()

		data, err := ioutil.ReadAll(res.Body)
		assert.NoError(t, err)
		assert.Equal(t, http.StatusNotImplemented, res.StatusCode)
		assert.JSONEq(t, `{"error": "Status: NotImplemented"}`, string(data))
	})

	t.Run("ok", func(t *testing.T) {
		w := httptest.NewRecorder()

		writeResponse(w, http.StatusOK, map[string]string{"abc": "123"})

		res := w.Result()
		defer res.Body.Close()

		data, err := ioutil.ReadAll(res.Body)
		assert.NoError(t, err)
		assert.Equal(t, http.StatusOK, res.StatusCode)
		assert.JSONEq(t, `{"abc": "123"}`, string(data))
	})
}

func TestNewServer(t *testing.T) {
	pointsMock := new(PointsMock)
	defer pointsMock.AssertExpectations(t)

	server := NewServer(pointsMock)
	assert.IsType(t, &Server{}, server)
	assert.Equal(t, pointsMock, server.points)
}

func TestServer_PointsHandler(t *testing.T) {
	t.Run("invalid distance", func(t *testing.T) {
		pointsMock := new(PointsMock)
		defer pointsMock.AssertExpectations(t)

		req := httptest.NewRequest(http.MethodGet, "/api", nil)
		w := httptest.NewRecorder()

		NewServer(pointsMock).PointsHandler(w, req)

		res := w.Result()
		defer res.Body.Close()

		data, err := ioutil.ReadAll(res.Body)
		assert.NoError(t, err)
		assert.Equal(t, http.StatusBadRequest, res.StatusCode)
		assert.JSONEq(t, `{"error": "invalid distance"}`, string(data))
	})

	t.Run("invalid x", func(t *testing.T) {
		pointsMock := new(PointsMock)
		defer pointsMock.AssertExpectations(t)

		req := httptest.NewRequest(http.MethodGet, "/api?distance=10", nil)
		w := httptest.NewRecorder()

		NewServer(pointsMock).PointsHandler(w, req)

		res := w.Result()
		defer res.Body.Close()

		data, err := ioutil.ReadAll(res.Body)
		assert.NoError(t, err)
		assert.Equal(t, http.StatusBadRequest, res.StatusCode)
		assert.JSONEq(t, `{"error": "invalid x"}`, string(data))
	})

	t.Run("invalid y", func(t *testing.T) {
		pointsMock := new(PointsMock)
		defer pointsMock.AssertExpectations(t)

		req := httptest.NewRequest(http.MethodGet, "/api?distance=10&x=5", nil)
		w := httptest.NewRecorder()

		NewServer(pointsMock).PointsHandler(w, req)

		res := w.Result()
		defer res.Body.Close()

		data, err := ioutil.ReadAll(res.Body)
		assert.NoError(t, err)
		assert.Equal(t, http.StatusBadRequest, res.StatusCode)
		assert.JSONEq(t, `{"error": "invalid y"}`, string(data))
	})

	t.Run("empty", func(t *testing.T) {
		pointsMock := new(PointsMock)
		pointsMock.
			On("Filter", Point{X: 5, Y: 8}, 10).
			Return([]PointWithDistance{})
		defer pointsMock.AssertExpectations(t)

		req := httptest.NewRequest(http.MethodGet, "/api?distance=10&x=5&y=8", nil)
		w := httptest.NewRecorder()

		NewServer(pointsMock).PointsHandler(w, req)

		res := w.Result()
		defer res.Body.Close()

		data, err := ioutil.ReadAll(res.Body)
		assert.NoError(t, err)
		assert.Equal(t, http.StatusOK, res.StatusCode)
		assert.JSONEq(t, `[]`, string(data))
	})

	t.Run("return list", func(t *testing.T) {
		pointsMock := new(PointsMock)
		pointsMock.
			On("Filter", Point{X: 5, Y: 8}, 10).
			Return([]PointWithDistance{{X: 5, Y: 5, Distance: 10}, {X: 2, Y: 2, Distance: 4}, {X: 3, Y: 3, Distance: 6}})

		defer pointsMock.AssertExpectations(t)

		req := httptest.NewRequest(http.MethodGet, "/api?distance=10&x=5&y=8", nil)
		w := httptest.NewRecorder()

		NewServer(pointsMock).PointsHandler(w, req)

		res := w.Result()
		defer res.Body.Close()

		data, err := ioutil.ReadAll(res.Body)
		assert.NoError(t, err)
		assert.Equal(t, http.StatusOK, res.StatusCode)
		assert.JSONEq(t, `[{"x":2, "y":2, "distance":4}, {"x":3, "y":3, "distance":6}, {"x":5, "y":5, "distance":10}]`, string(data))
	})
}
